import ext from "./utils/ext";
import open from "./open";
ext.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if(request.action === "open-request-create") {
      open(request.url, request.tabId, request.includeSession)
        .then(function () {
          sendResponse({ok: true});
        })
        .catch(function (err) {
          sendResponse({err: err});
        });

      return true;
    }
  }
);
