import ext from "./utils/ext";

try {
    // If changed, change in SanitizerSession.java too
    const MAX_LOCAL_STORAGE_SIZE = 1024 * 1024;
    const MAX_LOCAL_STORAGE_ITEM_SIZE = 1024;
    
    var discardedMaxSize = false;
    var discardedMaxItemSize = 0;
    var sizeCounter = 0;

    var localStorageResult = {};
    var localStorageKeys = Object.keys(window.localStorage);
    var localStorageIndex = localStorageKeys.length;
    while(localStorageIndex--) {
        var key = localStorageKeys[localStorageIndex];
        var value = window.localStorage.getItem(key);
        if(!value) {
            continue;
        }
        var itemSize = key.length + value.length;
        if(itemSize > MAX_LOCAL_STORAGE_ITEM_SIZE) {
            discardedMaxItemSize++;
            continue;
        }
        sizeCounter += itemSize;
        if(sizeCounter > MAX_LOCAL_STORAGE_SIZE) {
            discardedMaxSize = true;
            break;
        }
        localStorageResult[key] = value;
    }

    var sessionStorageResult = {};
    var sessionStorageKeys = Object.keys(window.sessionStorage);
    var sessionStorageIndex = sessionStorageKeys.length;
    while(sessionStorageIndex--) {
        var key = sessionStorageKeys[sessionStorageIndex];
        var value = window.sessionStorage.getItem(key);
        if(!value) {
            continue;
        }
        var itemSize = key.length + value.length;
        if(itemSize > MAX_LOCAL_STORAGE_ITEM_SIZE) {
            discardedMaxItemSize++;
            continue;
        }
        sizeCounter += itemSize;
        if(sizeCounter > MAX_LOCAL_STORAGE_SIZE) {
            discardedMaxSize = true;
            break;
        }
        sessionStorageResult[key] = value;
    }

    if(discardedMaxItemSize > 0 || discardedMaxSize) {
        console.log(`Discarded local storage due to limitations, items ${discardedMaxItemSize}, due to size ${discardedMaxSize}`)
    }

    var storage = [];
    if(localStorageResult.length !== 0 || sessionStorageResult !== 0) {
        // See BrowserRequest.java
        storage.push({
            url: window.location.href,
            localContent: localStorageResult,
            sessionContent: sessionStorageResult,
        })
    }

    ext.runtime.sendMessage({
        action: "local-storage-result",
        ok: true,
        storage: storage
    });
} catch(err) {
    ext.runtime.sendMessage({
        action: "local-storage-result",
        ok: false,
        err: JSON.stringify(err)
    });
}
