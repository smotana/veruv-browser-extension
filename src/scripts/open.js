import ext from "./utils/ext";

function extractUserAgent() {
  return new Promise(function (resolve) {
    resolve(navigator.userAgent);
  });
}
function extractLocalStorage(tabId) {
  return new Promise(function (resolve) {
    var timeout = setTimeout(function() {
      console.log("LocalStorage Extract script timed out");
      ext.runtime.onMessage.removeListener(listener);
      resolve();
    }, 1000);
    var listener = function(request, sender, sendResponse) {
      if(!sender.tab
        || sender.tab.id !== tabId) {
        return;
      }
      if(request.action === "local-storage-result") {
        resolve(request.storage);
        sendResponse({ok: true});
        ext.runtime.onMessage.removeListener(listener);
        clearTimeout(timeout);
      }
    };
    ext.runtime.onMessage.addListener(listener);
    ext.tabs.executeScript(tabId, {
      runAt: "document_start",
      file: 'scripts/extractLocalStorage.js'
    });
  });
}
// If changed, change in SanitizerSession.java too
const MAX_COOKIE_COUNT = 256;
const MAX_COOKIE_ITEM_SIZE = 4096;
function extractCookies(url) {
  return new Promise(function (resolve) {
    ext.cookies.getAll({url: url}, function(cookies) {
      if(!cookies) {
        return resolve();
      }
      if(cookies.length > MAX_COOKIE_COUNT) {
        console.log(`Truncating ${cookies.length - MAX_COOKIE_COUNT} cookies`);
        cookies = cookies.slice(0, MAX_COOKIE_COUNT);
      }
      var discardedMaxItemSize = 0;
      var cookiesResult = cookies
        .filter(function(cookie) {
          var cookieSize = 
            (cookie.domain && cookie.domain.length || 0 )
            + (cookie.name && cookie.name.length || 0 )
            + (cookie.value && cookie.value.length || 0 )
            + (cookie.path && cookie.path.length || 0 );
          if(cookieSize > MAX_COOKIE_ITEM_SIZE) {
            discardedMaxItemSize++;
            return false;
          }
          return true;
        })
        .map(function(cookie) {return {
          // See BrowserRequest.java
          i: cookie.hostOnly,
          d: cookie.domain,
          n: cookie.name,
          v: cookie.value,
          p: cookie.path,
          s: cookie.session,
          e: cookie.expirationDate,
          c: cookie.secure,
          h: cookie.httpOnly,
        }});
      if(discardedMaxItemSize > 0) {
        console.log(`Discarded ${discardedMaxItemSize} over sized cookies`);
      }
      resolve(cookiesResult);
    });
  });
}
function createRequest(url, tabId, includeSession) {
  if(!includeSession) {
    // See browserApi.BrowserSession type
    return Promise.resolve({
      url: url
    })
  }
  return Promise.all([
    extractLocalStorage(tabId),
    extractCookies(url),
    extractUserAgent()])
  .then(function(args) {
    var localStorage = args[0];
    var cookies = args[1];
    var userAgent = args[2];

    // See browserApi.BrowserSession type
    var session = {
      url: url,
      userAgent: userAgent,
      cookies: cookies,
      localStorage: localStorage,
    };

    return session;
  });
}
function openTab(url) {
  return new Promise(function (resolve, reject) {
      var timeout = setTimeout(function() {
          reject('Timed out waiting for page load');
      }, 3000);
      ext.tabs.create({
          active: true,
          url: url
      }, function(tab) {
          if(!tab) {
              return reject('failed to open tab: ' + url);
          }
          resolve(tab);
          clearTimeout(timeout);
      });
  });
}
function listenForOpenRequest(args) {
  var openRequest = args[0];
  var tab = args[1];

  return new Promise(function (resolve, reject) {
    var timeout = setTimeout(function() {
      reject('Timed out waiting for veruv');
      ext.runtime.onMessage.removeListener(listener);
    }, 5000);
    var listener = function(request, sender, sendResponse) {
      if(request.action === "open-request-query") {
        sendResponse({
          ok: true,
          request: openRequest,
        });
      } else if(request.action === "open-request-result") {
        if(request.ok) {
          resolve();
        } else {
          reject('Failed to process request');
        }
        ext.runtime.onMessage.removeListener(listener);
        clearTimeout(timeout);
      }
    };
    ext.runtime.onMessage.addListener(listener);
  });
}

function open(url, tabId, includeSession) {
  return Promise.all([
      createRequest(url, tabId, includeSession),
      openTab("https://veruv.com/create")
    ])
    .then(listenForOpenRequest);
}

export default open;
