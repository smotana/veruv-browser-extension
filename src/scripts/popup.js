import ext from "./utils/ext";

var popup = document.getElementById("app");
var activeTab = undefined;
var lastAlertBsStyle = undefined;

var showAlertCannotBeOpened = (text, bsStyle='danger') => {
  var alertEl = document.getElementById("alert-cannot-be-opened");
  alertEl.classList.remove("hidden")
}

var showFaq = () => {
  var faqContainerEl = document.getElementById("collapsable-faq");
  faqContainerEl.classList.remove("collapsable-closed")
}

var showAlert = (text, bsStyle='danger') => {
  var alertEl = document.getElementById("alert");
  var collapsableEl = document.getElementById("collapsable-alert");
  var styleClass = "alert-" + bsStyle;
  if(lastAlertBsStyle) {
    if(lastAlertBsStyle === styleClass) {
      // Nothing to do
    } else {
      alertEl.classList.add(styleClass);
      alertEl.classList.remove(lastAlertBsStyle);
      lastAlertBsStyle = styleClass;
    }
  } else {
    alertEl.classList.add(styleClass);
    lastAlertBsStyle = styleClass;
  }
  lastAlertBsStyle = "alert-" + bsStyle;
  alertEl.innerText = text;
  collapsableEl.classList.remove("collapsable-closed");
}

var hideAlert = () => {
  var collapsableEl = document.getElementById("collapsable-alert");
  collapsableEl.classList.add("collapsable-closed");
}

var enableOpenButton = (tab) => {
  var buttonOpen = document.getElementById("btn-open");
  var checkboxIncludeSession = document.getElementById("checkbox-include-session");
  activeTab = tab;
  buttonOpen.disabled = false;
  checkboxIncludeSession.disabled = false;
}

var render = (tab) => {
  if(!tab.url || !/^https?:\/\//.test(tab.url) || tab.url.lastIndexOf('https://chrome.google.com/webstore', 0) === 0) {
    showAlertCannotBeOpened();
    return;
  }
  enableOpenButton(tab);
}

ext.tabs.query({active: true, currentWindow: true}, function(tabs) {
  render(tabs[0]);
});

popup.addEventListener("click", function(e) {
  if(!e.target) {
    return;
  }
  if(e.target.matches("#btn-open") && activeTab) {
    e.preventDefault();
    var includeSessionCheckBox = document.getElementById("checkbox-include-session");
    var includeSession = false;
    if(includeSessionCheckBox) {
      includeSession = includeSessionCheckBox.checked;
    }
    ext.runtime.sendMessage({
      action: "open-request-create",
      url: activeTab.url,
      tabId: activeTab.id,
      includeSession: includeSession
    }, function(response) {
      if(response && response.ok) {
        window.close();
      } else {
        showAlert('Failed to open: ' + (response && response.err || 'Unknown error'));
      }
    });
  } else if(e.target.matches("#btn-help")) {
    showFaq();
  } else if(e.target.matches(".link")) {
    var link = e.target.getAttribute('link');
    if(link) {
      ext.tabs.create({'url': link});
    }
  }
});
