import ext from "./utils/ext";
import Bridge from "./bridge";

const bridge = new Bridge();

bridge.registerExtensionHandler(
  'ExtensionContentScript',
  // On connected handler
  function() {
    ext.runtime.sendMessage({action: "open-request-query"}, function(response) {
      if(response && response.ok && response.request) {
        bridge.sendMessageToSite('open', response.request);
      } else {
        console.log('Failed to open: ' + (response && response.err || 'Unknown error'));
      }
    });
  },
  // On message handler
  function(type, content) {
    if(type === 'open-ack') {
      ext.runtime.sendMessage({action: "open-request-result", ok:true});
    } else if(type === 'open-err') {
      ext.runtime.sendMessage({action: "open-request-result", err: content && content.err || 'Site sent back error'});
      }
  });
